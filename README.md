# FIRMWARE  -  AUTOMATION  DEMO VERSION

I/O Mapping on Beckhoff :

| DI |            | DO |         |
|----|------------|----|---------|
| 0  | iEmergency | 0  | oBuzzer |
| 1  | iReset     | 1  | oGreen  |
| 2  | iSelect    | 2  | oRed    |
| 3  | iStart     | 3  | oReset  |
| 4  | iStop      | 4  | oStart  |
| -  | -          | 5  | oYellow |


## VARIABLES COMPOSITION SUMMARY

* **Alarms**: written by CoDeSys Automation Firmware and read by Modbus.   
Memory data from Area 100: Emergency button with values 1 when pressed and 0 otherwise.   

* **General Parameters**: written by Modbus and read by CoDeSys Automation Firmware.  
Memory data from Area 200: Max velocity [bph].  

* **Product Parameters**: written by Modbus and read by CoDeSys Automation Firmware.  
Memory data from Area 300: Nominal velocity [bph].  

* **Results**: written by CoDeSys Automation Firmware and read by Modbus.  
Memory data from Area 400: ProductionSpeed [bph], SpeedPercentage [%], NominalSpeed [bph], MaxSpeed [bph], MachineState [ 0 – Emergency state, 1 – Stop, 2 – Automatic, 3 – Manual ], TotalRotationTime [h] [.#], PartialRotationTime [h] [.#], TotalBottles [bottles], PartialBottles [bottles].  

* **Diagnostics**: written by CoDeSys Automation Firmware and read by Modbus.  
Memory data from Area 500: machineInAlarm [BOOL], freeDiagnostics1, freeDiagnostics2, freeDiagnostics3, dataDiagnostics1, dataDiagnostics2, dataDiagnostics3, dataDiagnostics4.

* **Commands**: written by Modbus and read by CoDeSys Automation Firmware.  
Memory data from Area 600: ResetAlarms [BOOL], mFreeArea1, mResetResults [Reset sequence 1124], mFreeArea2.

A sidenote: we made the algorithm working with the Holding Register directly without having to work on 2 different vectors for I/O and make them overlapping on I/O Modbus settings.

## Libraries to be included for using this project

Add the Arol Base Package ver. 1.0.0 before starting this project. The function is called from this package to calculate the interval in which the algorithm works.

You may download the library from
<https://bitbucket.arol.ibuildings.cloud/projects/FWA/repos/arol-fwa-binary/browse/Libraries/Codesys/ArolBase>
(you need a valid login)

## Code optimization

Each method was designed to reduce the complexity of the main program and facilitate its readability. The methods implemented are ResetCommands, UpdateModbus, UpdatePersistent and UpdateResults.

* ResetCommands: If the sequence '1124' (choosen by us) is inserted by modbus at the register data area n° 600, the algorithm will reset the alarms. If the same sequence is written at the register data area n° 602 the algorithm will reset the partial persistent variables that have the partial functional time of the machine and the partial number of bottle processed.
* UpdateModubs: It will copy the variable from modbus General and Nominal parameters to local data and it will copy the Results from the local data to the holding registers.
* UpdatePersistent: This part of the algorithm assign the persisten variables to the local data section and viceversa.
* UpdateResults: This method update the Maximum and Nominal speed in the Results part of the local data to maintain those value in case the Modbus connection goes down for some reason. So the algorithm can continue to work without any problem.

## How to Start the firmware

Once you have installed the library and opened the Automation firmware in CoDeSys you have to check if the deployment platform is online. Click on Device at the top of the Tree, make sure your Gateway for CoDeSys is inserted right then try inserting the IP of your destination device where you want the algorithm to work.

![Picture: Device Connected](images/DeviceConnected.png)

Once you have all green lights on those parts, click on the *Login* icon or alternatively press *Alt + F8*. This will tell the IDE to recompile the whole project and if you won't have any error, you will successfully go **ONLINE**.

When online, click on the *Start/Play* icon or alternatively press *F5*.

The algorithm was created with a simulated machine implemented in the *MachineState* which covers the basic functions of any Arol S.p.A. machine: `Emergency`, `Stop`, `RunAutomatic` and `RunManual`. This *MachineState* is called inside the main program in the variable **machine**.
In order to work with the automation algorithm you have to make changes to this variable to change the machine state and see what is computed by the Automation firmware.

![alt text](images/Machinestatevariables.png)

If you have got the EtherCAT module connected to it, once you are online, you can use the *RESET* button to move the machine from **Emergency State** to the **Stop State**. Then you can press the *START* button after you have selected the key for *Automatic or Manual mode*. The machine will move to **Running State** until you press the *STOP* button, it will move back to Stop State, or until you press the *Emergency* button, it will move back to the Emergency State. You can even change the running mode key by moving between Automatic and Manual mode while the machine is Running. Once detected this change the machine will move back automatically in STOP state.

If you have no EtherCAT module you must disable the EtherCAT_Master interface in the project and delete the relative *TASK* near the main program **PLC_PRG**. Also click with the right mouse button on the *EtherCAT_Master*, select *Properties* then move to *Build* and check on *Exclude from build*(be advised that if you connect an EtherCAT module, you will have to undo all those steps). In this configuration you have to open the *machine* variable and insert the same commands as explained before. You should know that the *RESET* button works on a fall trigger while the *START* button works on a rise trigger.
